package structures;

public class BinTree {
    public class Node {
        int value;
        Node left;
        Node right;

        public Node(int value) {
            this.value = value;
        }

        public Node() {
        }
    }

    Node root;

    public BinTree(Node root) {
        this.root = root;
    }

    public BinTree(int root) {
        this.root = new Node(root);
    }

    public BinTree() {
        this.root = new Node();
    }

    public void insert(Node node, int value) {
        if (node == null) {
            node = new Node(value);
        } else {
            if (value < node.value) {
                if (node.left == null) {
                    node.left = new Node(value);
                } else insert(node.left, value);
            } else {
                if (node.right != null)
                    insert(node.right, value);
                else
                    node.right = new Node(value);
            }
        }
    }


    public static void main(String[] args) {
        int[] numeros = {0, 56, 1, 41, 2,42,57,58,90};
        BinTree tree = new BinTree(10);
        for (int i : numeros) {
            tree.insert(tree.root, i);
        }

        tree.preOrdem(tree.root);
        System.out.println();
        tree.ordem(tree.root);
        int h = tree.altura(tree.root);
        System.out.println();
        System.out.println(h);

    }

    private int altura(Node node) {
        int h = 0;
        if(node != null){
            h = 1;
            h += altura(node.left) > altura(node.right) ?  altura(node.left) : altura(node.right);
        }
        return h;
    }

    private void ordem(Node node) {
        if (node != null) {
            ordem(node.left);
            System.out.print(node.value + " ");
            ordem(node.right);
        }
    }

    private void preOrdem(Node node) {
        if (node != null) {
            System.out.print(node.value + " ");
            preOrdem(node.left);
            preOrdem(node.right);
        }
    }


}
