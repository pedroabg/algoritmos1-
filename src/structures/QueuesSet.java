package structures;

import java.util.*;

public class QueuesSet {

    public static void main(String args[]){
        Queue<Integer> q1 = new LinkedList<Integer>();
        q1.add(3);
        q1.add(2);
        q1.add(4);
        System.out.println(q1.size());

        while (q1.peek() != null){
            System.out.println(q1.poll());
        }

        System.out.println();
        PriorityQueue<Integer> p = new PriorityQueue<>( Collections.reverseOrder());
        p.add(4);
        p.add(1);
        p.add(3);
        p.add(2);
        p.add(5);



        Iterator itr = p.iterator();
        while (itr.hasNext())
            System.out.println(itr.next());
        System.out.println();

        System.out.println(p.poll());
        System.out.println(p.contains(2));

        System.out.println("Sets");
        Set<Integer> carros =  new HashSet<Integer>();
        carros.add(4);
        carros.add(4);
        carros.add(5);
        System.out.println(carros.contains(5));
        Iterator it = carros.iterator();
        while (it.hasNext())
            System.out.println(it.next());


    }

}
