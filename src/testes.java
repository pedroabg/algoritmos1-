import java.util.Scanner;
import java.util.Stack;

public class testes {
    public static boolean check(char c1,char c2){

        return ((c1 == '(') && (c2 == ')')) || ((c1 == '[') && (c2 == ']')) || ((c1 == '{') && (c2 == '}'));
    }

    public static void main(String[] args) {
//|| (c1 == ']' &&   s.charAt(i-1) == '[') || ( c1 == ')' && s.charAt(i-1) == '(' ) )
        String s = "{{[(()([]))]}}";
        //String s = "{{}{}}";
        Stack<Character> pilha = new Stack();
        char c1,c2;
        int top = -1;

        for (int i = 0; i < s.length(); i++) {
               c1 =  s.charAt(i);
               pilha.push(c1);
               top = pilha.size()-2;

               if ((c1 == '}' && ( top >-1 &&  pilha.elementAt(top) == '{') ) ||
                       (c1 == ']' && ( top >-1 &&  pilha.elementAt(top) == '[') ) ||
                       (c1 == ')' && ( top >-1 &&  pilha.elementAt(top) == '(') )
               ) {
                   pilha.pop();
                   pilha.pop();
               }

        }

        if(pilha.empty())
            System.out.print("YES");
        else
            System.out.print("NO");
    }

}



/*    String s = "{{[(()([]))]}}";
    Stack<Character> pilha = new Stack();
    char c1,c2;

        for (int i = 0; i < s.length(); i++) {
        c1 =  s.charAt(i);
        if (c1 == '{' || c1 == '['  || c1 == '(' )
        pilha.push(c1);
        else {
        c2 = pilha.pop();
        if(!check(c2,c1)){
        System.out.print("NO");
        break;
        }

        }
        }

        System.out.print("YES");




               Stack<Character> pilha = new Stack();
        char c1,c2;

        for (int i = 0; i < s.length(); i++) {
               c1 =  s.charAt(i);

               if (c1 == '{' || c1 == '['  || c1 == '(' )
                   pilha.push(c1);

               else {
                 c2 = pilha.pop();
                 if(!(((c2 == '(') && (c1 == ')')) || ((c2 == '[') && (c1 == ']')) || ((c2 == '{') && (c1 == '}')))){
                     System.out.print("NO");
                     break;
                 }

               }
        }





        */
