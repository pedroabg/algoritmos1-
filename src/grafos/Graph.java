package grafos;

import java.util.ArrayList;
import java.util.List;

public class Graph {

    List nodes[];
    int size;

    public void addEdge(int first, int second) {
        nodes[first].add(second);
        nodes[second].add(first);
    }

    public Graph(int size) {
        this.nodes = new List[size];
        this.size = size;
        nodes = init(nodes);
    }

    private List[] init(List[] nodes) {
        for (int i = 0; i < nodes.length; i++) {
            nodes[i] = new ArrayList<Integer>();
        }
        return nodes;
    }

    public List getNeigbors(int u){

        return copy(nodes[u]);
    }
    public List copy(List<Integer> node){
        List<Integer> copy = new ArrayList<Integer>();
        for (int i = 0; i < node.size(); i++) {
            copy.add(node.get(i));
        }
        return copy;
    }

}

//
//public static class Node {
//    private int value;
//    private List<Node> neighbors;
//
//    public Node(int value) {
//        this.value = value;
//        this.neighbors = new ArrayList<Node>();
//    }
//
//    public int[] shortestReach(int startId) { // 0 indexed
//
//        return null;
//    }
//
//    public int getValue() {
//        return value;
//    }
//
//    public void setValue(int value) {
//        this.value = value;
//    }
//
//
//}