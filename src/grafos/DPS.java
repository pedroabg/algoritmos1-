package grafos;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class DPS {
    public static final byte white = 0;
    public static final byte gray = 1;
    public static final byte black = 2;
    public int d[], pre[];
    private SimpleGraph graph;

    public DPS(SimpleGraph graph) {
        this.graph = graph;
        int n = this.graph.size;
        this.d = new int[n];
        this.pre = new int[n];
    }

    private void visitaDfs(int u, int cor[]) throws Exception {
        cor[u] = black; this.d[u] = 0;
        Stack<Integer> stk = new Stack();
        stk.push(u);
        int a;

        while (!stk.isEmpty()) {

            int unvisited = getUnvisitedNeigbor(stk.peek(),cor);
            if (unvisited == -1) // todos já foram visitados
                stk.pop();
            else {
                cor[unvisited] = black;
                pre[unvisited] = stk.peek();
                stk.push(unvisited);
            }


//      System.out.print("Visita "+u+" dist: "+this.d[u]+" cor: preto F:");
//      fila.imprime ();
        }
    }

    public void buscaEmProfundidade() throws Exception {
        int n = this.graph.size;
        int cor[] = new int[n];

        for (int u = 0; u < n; u++) {
            cor[u] = white; this.d[u] = Integer.MAX_VALUE;
            this.pre[u] = -1;
        }
        for (int u = 0; u < n; u++)
            if (cor[u] == white) this.visitaDfs(u, cor);
    }

    public int getUnvisitedNeigbor(int peek, int[] cor) {
        for (int i = 0; i < graph.matrix.length; i++){
            if(graph.matrix[peek][i] > 0 && cor[i] == white)
                return i;
        }
        return -1;
    }





    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        int queries = scanner.nextInt();

        for (int t = 0; t < queries; t++) {

            // Create a graph of size n where each edge weight is 6:
            SimpleGraph graph = new SimpleGraph(scanner.nextInt());
            int m = scanner.nextInt();

            // read and set edges
            for (int i = 0; i < m; i++) {
                int u = scanner.nextInt() - 1;
                int v = scanner.nextInt() - 1;

                // add each edge to the graph
                graph.addEdge(u, v, 1);
            }

            // Find shortest reach from node s
            int startId = scanner.nextInt() - 1;
            DPS bfs = new DPS(graph);
            bfs.buscaEmProfundidade();

            print(bfs.d);
            //System.out.println(bfs.pre);
        }

        scanner.close();
    }


    public static void print(int[] r){
        for (int i = 0; i < r.length; i++) {
            System.out.print(r[i]+" ");
        }
    }

}
/*

1
6 5
1 2
1 5
2 5
2 4
5 3
1
*/
