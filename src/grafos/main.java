package grafos;

public class main {

    public static void main(String[] args) throws Exception {
/*        int[][] arestas = {
                {1,2,1},
                {3,1,1},
                {4,1,5},
                {4,3,2}
        };     */
        int[][] arestas = {
                {1,2,1},
                {3,1,1},
                {4,1,5},
                {4,3,2},
                {5,3,1},
                {4,5,1}
        };

        SimpleGraph graph = new SimpleGraph(arestas.length);
        SimpleGraph result;
        Prim prim = new Prim(graph);

        for(int[] line: arestas){
                graph.addEdge(--line[0],--line[1],line[2]);
        }

       // System.out.print(graph);

        result = prim.startPrim();

        //System.out.print(result);

        BFS bfs = new BFS(graph);
        bfs.buscaEmLargura();

        bfs.print(bfs.pre);
        System.out.println("");
        bfs.print(bfs.d);

        DPS dps = new DPS(graph);
        dps.buscaEmProfundidade();
        System.out.println("");
        System.out.println("Profundidade");
        dps.print(dps.pre);
        System.out.println("");
        dps.print(dps.d);


    }
}
