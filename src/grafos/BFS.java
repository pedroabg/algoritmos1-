package grafos;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BFS {
    public static final byte white = 0;
    public static final byte gray = 1;
    public static final byte black = 2;
    public int d[], pre[];
    private SimpleGraph graph;

    public BFS(SimpleGraph graph) {
        this.graph = graph;
        int n = this.graph.size;
        this.d = new int[n];
        this.pre = new int[n];
    }

    private void visitaBfs (int u, int cor[]) throws Exception {
        cor[u] = gray; this.d[u] = 0;
        List<Integer> fila = new ArrayList<Integer>();
        fila.add(u);
        int adj,a;
//    System.out.print ("Visita origem:"+u+" cor: cinza F:");
//    fila.imprime ();
        while (!fila.isEmpty()) {
            u = fila.remove(fila.size()-1);
            List<Integer> neighbors = this.graph.getNeigbors(u);

            while (!neighbors.isEmpty()) {
                a = neighbors.remove(0);

                int v = a;
                if (cor[v] == white) { // quem tá na fila tem a cor cinza
                    cor[v] = gray; this.d[v] = this.d[u] + 1;
                    this.pre[v] = u;
                    fila.add (v);
                }

            }
            cor[u] = black;
//      System.out.print("Visita "+u+" dist: "+this.d[u]+" cor: preto F:");
//      fila.imprime ();
        }
    }

    public void buscaEmLargura () throws Exception {
        int n = this.graph.size;
        int cor[] = new int[n];

        for (int u = 0; u < n; u++) {
            cor[u] = white; this.d[u] = Integer.MAX_VALUE;
            this.pre[u] = -1;
        }
        for (int u = 0; u < n; u++)
            if (cor[u] == white) this.visitaBfs (u, cor);
    }





    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        int queries = scanner.nextInt();

        for (int t = 0; t < queries; t++) {

            // Create a graph of size n where each edge weight is 6:
            SimpleGraph graph = new SimpleGraph(scanner.nextInt());
            int m = scanner.nextInt();

            // read and set edges
            for (int i = 0; i < m; i++) {
                int u = scanner.nextInt() - 1;
                int v = scanner.nextInt() - 1;

                // add each edge to the graph
                graph.addEdge(u, v, 1);
            }

            // Find shortest reach from node s
            int startId = scanner.nextInt() - 1;
            BFS bfs = new BFS(graph);
            bfs.buscaEmLargura();

            print(bfs.d);
            //System.out.println(bfs.pre);
        }

        scanner.close();
    }


    public static void print(int[] r){
        for (int i = 0; i < r.length; i++) {
            System.out.print(r[i]+" ");
        }
    }

}
/*

1
6 5
1 2
1 5
2 5
2 4
5 3
1
*/
