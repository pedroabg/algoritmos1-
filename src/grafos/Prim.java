package grafos;

public class Prim {
    boolean [] verified;
    int [] pred;
    int [] relativeCost;
    SimpleGraph result,graph;
    int totalCost;

    public Prim(SimpleGraph graph) {
        this.graph = graph;
        this.pred = new int[graph.size];
        this.relativeCost = new int[graph.size];
        this.verified = new boolean[graph.size];
        this.result = new SimpleGraph(graph.size);
        this.totalCost = 0;
    }

    public SimpleGraph startPrim(){
        int size = graph.size;
        int [][] result = new int[size][size];
        int [][] matrix = graph.matrix;

        //Set up
        for(Integer count = 0; count < size; count++){
            verified[count] = false;
            relativeCost[count] = Integer.MAX_VALUE;
        }

        // Start node
        relativeCost[0] = 0;
        int currNode = 0;



        for(Integer contadorPontosAvaliados = 0; contadorPontosAvaliados < size; contadorPontosAvaliados++){
            for(Integer contadorVizinhos = 0; contadorVizinhos < size; contadorVizinhos++){

                /**
                 * Verifica se o nó a ser avaliado nesta iteração já foi avaliado anteriormente;
                 * se sim, passa para a próxima iteração
                 */
                if( verified[contadorVizinhos] || (contadorVizinhos == currNode)){
                    continue;
                }

                /**
                 * Duas comparações aqui:
                 *
                 * 1ª - Verifica se na matrizSistema há algum valor na coluna que seja > 0. Caso afirmativo,
                 * significa que há uma aresta entre estes dois pontos do grafo.
                 *
                 * 2ª - Verifica se o peso da aresta entre os dois nós é menor que a atual distanciaRelativa
                 * do nó vizinho.
                 *
                 * Caso correto, a distanciaRelativa do nó vizinho ao que está sendo avaliado no momento será
                 * atualizada pelo valor dessa nova distancia avaliada até o pontoAvaliado.
                 */

                if((matrix[currNode][contadorVizinhos] > 0) && ((matrix[currNode][contadorVizinhos] <
                                relativeCost[contadorVizinhos]))){

                    //distanciaRelativa.set(contadorVizinhos, matrizSistema[pontoAvaliado][contadorVizinhos]);
                    relativeCost[contadorVizinhos] = matrix[currNode][contadorVizinhos];

                    //nosVizinhos.set(contadorVizinhos, pontoAvaliado);
                    pred[contadorVizinhos] = currNode;
                }
            }

            /**
             * Marca o vértice de pontoAvaliado como um vértice já verificado
             */
            //verticesVerificados.set(pontoAvaliado, true);
            verified[currNode] = true;

            /**
             * Preparação para seleção do próximo vértice a ser avaliado
             */
            currNode = 0;
            int minDist = new Integer(Integer.MAX_VALUE);

            /**
             * Seleção do próximo vértice a ser avaliado
             */
            for(Integer contador = 1; contador < verified.length; contador++){

                /**
                 * Se o vertice a ser verificado já foi verificado anteriormente (true)
                 * passa à próxima iteração.
                 */
                if(verified[contador]){
                    continue;
                }

                /**
                 * Se a distância relativa desse ponto for menor que a do ponto avaliado
                 * assume-se esse novo ponto como o ponto avaliado.
                 *
                 * Ao final da iteração, será selecionado o ponto com menor
                 * distância relativa.
                 */
                if(relativeCost[contador] < minDist){
                    minDist = relativeCost[contador];
                    currNode = contador;
                }

            }

        }

        /**
         * Criação da matrizResposta com a árvore resultante do Algoritmo de Prim
         */

        for(int contador = 1; contador < size; contador++){
            result[contador][pred[contador]] = matrix[contador][pred[contador]];
            result[pred[contador]][contador] = matrix[pred[contador]][contador];
        }


        this.result.matrix = result;
        return this.result;
    }

}
