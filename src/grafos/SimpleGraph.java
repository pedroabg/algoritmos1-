package grafos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SimpleGraph {
    int matrix [][];
    int size;

    public SimpleGraph(int size){
        matrix = new int[size][size];
        this.size = size;
    }

    public void addEdge(int a, int b, int c){
        this.matrix[a][b] = c;
        this.matrix[b][a] = c;
    }

    @Override
    public String toString() {
        String table = "";
        String cols = "";
        int p = 100/(size+1);
        table += " ";
        for (int i = 1; i <= size; i++) {
            table += " "+i;
        }
        table += "\n";
        for (int i = 0; i < size; i++) {
            table += (i+1)+" ";
            for (int j = 0; j < size; j++) {
                table += matrix[i][j]+" ";
            }
            table += "\n";
        }


        return table;
    }

    public List<Integer> getNeigbors(int u) {
        List<Integer> neighbors = new ArrayList();
        for (int j = 0; j < size; j++) {
            if (this.matrix[u][j] > 0){
                neighbors.add(j);
            }
        }
        return neighbors;
    }


}
