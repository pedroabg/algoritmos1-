package HashMapSet;

import java.util.Hashtable;
import java.util.Set;

public class HTable {
    public static void main(String[] arg) {
        Hashtable<Integer, String> h =
                new Hashtable<Integer, String>();

        Hashtable<Integer, String> h1 =
                new Hashtable<Integer, String>();

        h.put(3, "Geeks");
        h.put(2, "forGeeks");
        h.put(1, "isBest");


        // create a clone or shallow copy of hash table h
        h1 = (Hashtable<Integer, String>) h.clone();

        // checking clone h1
        System.out.println("values in clone: " + h1);

        // clear hash table h
        h.clear();

        // checking hash table h
        System.out.println("after clearing: " + h);

        // creating set view for hash table
        Set s = h1.entrySet();

        // printing set entries
        System.out.println("set entries: " + s);
        System.out.println(h1.get(3));

        for (int i : h1.keySet()) {
            System.out.println(i);
        }
        for (String i : h1.values()) {
            System.out.println(i);
        }
        h.put(3, "Cinco");

        h1.putAll(h);
        System.out.println(h1);
    }
}
