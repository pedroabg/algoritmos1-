
import java.io.*;
        import java.math.*;
        import java.security.*;
        import java.text.*;
        import java.util.*;
        import java.util.concurrent.*;
        import java.util.regex.*;

public class SolutionBalanced {

    public static boolean check(char c1,char c2){
        if (c1 == '(')
            return c2 == ')';
        if (c1 == '[')
            return c2 == ']';
        if (c1 == '{')
            return c2 == '}';

        return false;
    }

    // Complete the isBalanced function below.
    static String isBalanced(String s) {
        int middle = s.length()/2;
        String s1 = s.substring(0,middle);
        String s2 = s.substring(middle,s.length());

        for (int i = 0; i < middle; i++) {
            if (!check(s1.charAt(middle -i -1),s2.charAt(i) )){
                return "NO";

            }
        }

        return "YES";

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            String s = scanner.nextLine();

            String result = isBalanced(s);

            bufferedWriter.write(result);
            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
    }
}
