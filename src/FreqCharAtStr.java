public class FreqCharAtStr {
    public static void main(String[] args){
        String s = "PedroAlberto";
        int[] f = freqS(s.toLowerCase().toCharArray());

 }

    private static int[] freqS(char[] s) {
        int[] freq = new int[26];
        for (int i = 0; i < s.length; i++) {
            freq[s[i] - 'a']++;
        }
        for (int i = 0; i < freq.length; i++) {
            char c = (char)('a'+ i);
            System.out.println( c +" : " + freq[i]);
        }

        return freq;
    }
}
