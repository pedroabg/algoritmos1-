import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    public static class BFS {
        public static final byte white = 0;
        public static final byte gray = 1;
        public static final byte black = 2;
        private int d[], pre[];
        private Graph graph;

        public BFS(Graph graph) {
            this.graph = graph;
            int n = this.graph.size;
            this.d = new int[n];
            this.pre = new int[n];
        }


        private void visitaBfs(int u, int cor[]) throws Exception {
            cor[u] = gray;
            this.d[u] = 0;
            List<Integer> fila = new ArrayList<Integer>();
            fila.add(u);
            int adj, i, a;
//    System.out.print ("Visita origem:"+u+" cor: cinza F:");
//    fila.imprime ();
            while (!fila.isEmpty()) {
                u = fila.remove(fila.size() - 1);
                List<Integer> neighbors = this.graph.getNeigbors(u);
                i = 0;
                while (!neighbors.isEmpty()) {
                    a = neighbors.remove(0);

                    int v = a;
                    if (cor[v] == white) {
                        cor[v] = gray;
                        this.d[v] = this.d[u] + 1;
                        this.pre[v] = u;
                        fila.add(v);
                    }

                }
                cor[u] = black;
//      System.out.print("Visita "+u+" dist: "+this.d[u]+" cor: preto F:");
//      fila.imprime ();
            }
        }

        public void buscaEmLargura(int startId) throws Exception {
            int n = this.graph.size;
            int cor[] = new int[n];

            for (int u = 0; u < n; u++) {
                cor[u] = white;
                this.d[u] = Integer.MAX_VALUE;
                this.pre[u] = -1;
            }
            for (int u = 0; u < n; u++)
                if (cor[u] == white) this.visitaBfs (u, cor);
            //this.visitaBfs(startId, cor);
        }

    }


    public static class Graph {


        List nodes[];
        int size;

        public void addEdge(int first, int second) {
            nodes[first].add(second);
            nodes[second].add(first);
        }

        public Graph(int size) {
            this.nodes = new List[size];
            this.size = size;
            nodes = init(nodes);
        }

        private List[] init(List[] nodes) {
            for (int i = 0; i < nodes.length; i++) {
                nodes[i] = new ArrayList<Integer>();
            }
            return nodes;
        }

        public List getNeigbors(int u){

            return copy(nodes[u]);
        }
        public List copy(List<Integer> node){
            List<Integer> copy = new ArrayList<Integer>();
            for (int i = 0; i < node.size(); i++) {
                copy.add(node.get(i));
            }
            return copy;
        }


        public int[] shortestReach(int startId) throws Exception { // 0 indexed

            int[] d = new int[size];


            BFS bfs = new BFS(this);
            bfs.buscaEmLargura(startId);

            for (int i = 0; i <  bfs.d.length; i++) {
                d[i] = bfs.d[i] * 6;
                if(d[i] == 0) d[i] = - 1;

            }

            return d;
        }


    }


    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);

        int queries = scanner.nextInt();

        for (int t = 0; t < queries; t++) {

            // Create a graph of size n where each edge weight is 6:
            Graph graph = new Graph(scanner.nextInt());
            int m = scanner.nextInt();

            // read and set edges
            for (int i = 0; i < m; i++) {
                int u = scanner.nextInt() - 1;
                int v = scanner.nextInt() - 1;

                // add each edge to the graph
                graph.addEdge(u, v);
            }

            // Find shortest reach from node s
            int startId = scanner.nextInt() - 1;
            int[] distances = graph.shortestReach(startId);

            for (int i = 0; i < distances.length; i++) {
                if (i != startId) {
                    System.out.print(distances[i]);
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

        scanner.close();
    }
}

/*
1
4 2
1 2
1 3
1
*/
