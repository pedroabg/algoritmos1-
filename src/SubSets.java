import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SubSets {
    public static void main(String[] args) {
        char[] set = "abcd".toCharArray();
        List<Character>[] sets = new ArrayList[16];
        int n = set.length;
        int num = 2;
        char[] subset = Arrays.copyOfRange(set, 1, set.length);
//       System.out.println(1 << 1); // 0000010
//        System.out.println(1 << 2); // 0000100
//        System.out.println(1 << 3); // 0001000

        for (int i = 0; i < Math.pow(2,4); i++) {
            System.out.println("Conjunto "+i+" : ");
            //System.out.println(1 << i);
            sets[i] = new ArrayList<Character>();
            for (int j = 0; j < n; j++) {
                if ((i & (1 << j)) > 0){
                    System.out.println("entra o "+j);
                    sets[i].add(set[j]);
                }
            }
            //System.out.print(set[i] + " ");
        }

        System.out.print(sets);
        // Run a loop for printing all 2^n
        // subsets one by obe
//        for (int i = 0; i < (1 << n); i++) {
//            System.out.print("{ ");
//
//            // Print current subset
//            for (int j = 0; j < n; j++) {
//
//                // (1<<j) is a number with jth bit 1
//                // so when we 'and' them with the
//                // subset number we get which numbers
//                // are present in the subset and which
//                // are not
//
//                if ((i & (1 << j)) > 0)
//                    System.out.print(set[j] + " ");
//
//                System.out.println("}");
//            }
//        }
    }
}
