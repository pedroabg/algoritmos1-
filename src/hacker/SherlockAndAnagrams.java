//https://www.hackerrank.com/challenges/sherlock-and-anagrams/
package hacker;
import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;


public class SherlockAndAnagrams {
    // Complete the sherlockAndAnagrams function below.
    static int sherlockAndAnagrams(String s) {
        int n = s.length();
        double nSets = Math.pow(2,n);
        char[] chars = s.toCharArray();
        int[] sinature;
        Hashtable<String, Integer> sigFreq = new Hashtable<>();
        String subStr;
        sinature = new int[26];

        for (int i = 1; i < nSets; i++) {
            subStr = "";
            sinature = new int[26];
            for (int j = 0; j < n; j++) {
                if( (i & (1 << j)) > 0){
                    subStr += chars[j];
                    sinature[chars[j]-'a']++;
                }
            }
            // Check substring signature
            //Add to my signature map
            if(sigFreq.containsKey(Arrays.toString(sinature))){
                sigFreq.put(Arrays.toString(sinature), sigFreq.get(Arrays.toString(sinature)) +1);
            }else{
                sigFreq.put(Arrays.toString(sinature),1);
            }
            System.out.println(subStr);

        }

        int pairs = 0;
        for (String key : sigFreq.keySet()) {
            int sigF =sigFreq.get(key);
            if(sigF > 0)
                if (sigF > 1 )
                    pairs += (sigF)*(sigF-1)/2 -1;

        }


        return pairs;
    }

    public static void main(String[] args) throws IOException {

        int q = 1;

        for (int qItr = 0; qItr < q; qItr++) {
//            String s = scanner.nextLine();
            String s = "ifailuhkqq";

            int result = sherlockAndAnagrams(s);
            System.out.println(result);



        }

    }

}
