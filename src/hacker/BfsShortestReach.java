//https://www.hackerrank.com/challenges/ctci-bfs-shortest-reach/problem
package hacker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class BfsShortestReach {

    public static class Graph{
        private int[][] mat;



        public Graph(int n){
            this.mat = new int[n][n];
        }
        public int[][] getMat(){
            return this.mat;
        }
        public void addEdge(int u, int v){
            this.mat[u][v] = 6;
            this.mat[v][u] = 6;
        }

        public List getNeighbors(int s, int[] pred) {
            List l = new ArrayList<Integer>();
            for (int i = 0; i < this.mat.length; i++) {
                if(this.mat[s][i] > 0){
                    l.add(i);
                    if(pred[i] == -1)
                        pred[i] = s;
                }

            }
            return l;
        }
    }

    /*The first line contains an integer, , the number of queries.

    Each of the following  sets of lines is as follows:

    The first line contains two space-separated integers, n and m, the number of nodes and the number of edges.
    Each of the next  lines contains two space-separated integers, u and v, describing an edge connecting node u to node v .
    The last line contains a single integer,s , the index of the starting node.*/

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanner = new Scanner(System.in);

        Graph g;

        int u,v,s,m,n;
        int q = scanner.nextInt();

        for (int i = 0; i < q; i++) {
            m = scanner.nextInt();
            n = scanner.nextInt();
            g = new Graph(m);
            for (int j = 0; j < n; j++) {
                u = scanner.nextInt() - 1;
                v = scanner.nextInt() - 1;
                g.addEdge(u,v);
            }
            s = scanner.nextInt() - 1;
            distance(s,g);

        }
    }

    private static void distance(int s, Graph g) {
        int node;
        List<Integer> visited = new ArrayList<Integer>();
        List<Integer> queue = new ArrayList<Integer>();
        int[] pred = new int[g.mat.length];
        List neighbors;
        Arrays.fill(pred, -1);
        queue.add(s);
        while (!queue.isEmpty()){
            node = queue.remove(0);
            if(visited.contains(node)){
                continue;
            }else{
                visited.add(node);
            }
            neighbors = g.getNeighbors(node,pred);
            queue.addAll(neighbors);
        }
        //getNeigborg        // put in queue        // tira da fila,        // mark predecessor        // put neighbors in the end        // repeat until empty queue

        // for each node , back until S adding +6
        String solution = "";
        for (int i = 0; i < g.mat.length; i++) {
            int v = i;
            int distance = 0;
            if(i == s) continue;
            while(v != s && v != -1){
                v = pred[v];
                distance += 6;
            };

            solution += v == s ? Integer.toString(distance)+" " : "-1 ";
        }
        solution = solution.substring(0,solution.length()-1);
        System.out.println(solution);
    }
}
