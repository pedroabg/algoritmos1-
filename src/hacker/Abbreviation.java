package hacker;
//https://www.hackerrank.com/challenges/abbr/


import java.util.Arrays;
import java.util.List;

public class Abbreviation {

    // Complete the abbreviation function below.
    static String abbreviation(String a2, String b2) {
        String ans = "NO";
        char[] a = a2.toCharArray();
        char[] b = b2.toCharArray();
        int ai = 0;
        int bi = 0;

        if (a.length < b.length)
            return "NO";

        while (1 == 1) {
            // a acaba
            if (ai == a.length){
                if (bi == b.length){
                    ans = "YES";
                }
                break;
            }
            // b acaba
            if (bi == b.length){
                if (a[ai] >= 'A' && a[ai] < 'Z') {
                    break;
                } else {
                    ai++;
                    continue;
                }
            }

            if (a[ai] == b[bi] || (a[ai] - 'a' + 'A') == b[bi]) {
                a[ai] = b[bi];
                ai++;
                bi++;
            } else {
                if (a[ai] >= 'A' && a[ai] < 'Z') {
                    break;
                } else {
                    ai++;
                }
            }
        }
        return ans;
    }


    public static void main(String[] args) {

        String a = "Abcde";
        String b = "ABD";
        String result = abbreviation(a, b);
        System.out.println(result);

        a = "Pi";
        b = "P";
        result = abbreviation(a, b);
        System.out.println(result);

    }

}
