package sort;

public class Quick {
	/* low  --> Starting index,  high  --> Ending index */
	public static void quickSort(int arr[], int low, int high)
	{
		if (low < high)
		{
			/* pi is partitioning index, arr[pi] is now
	           at right place */
			int pi = partition(arr, low, high);

			quickSort(arr, low, pi - 1);  // Before pi
			quickSort(arr, pi + 1, high); // After pi
		}
	}

	public static int partition (int arr[], int low, int high)
	{
		// pivot (Element to be placed at right position)
		int pivot = arr[high];  

		int i = (low - 1);  // Index of smaller element
		int temp;

		for (int j = low; j <= high- 1; j++)
		{
			// If current element is smaller than or
			// equal to pivot
			if (arr[j] <= pivot)
			{
				i++;    // increment index of smaller element
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;

			}
			// ex: m3 .1 6 4 7 8 2 5
			// ex: 3 m1 .6 4 7 8 2 5
			// ex: 3 1 m6 .4 7 8 2 5
			// ex: 3 1 m4 .6 7 8 2 5
			// ex: 3 1 m4 6 .7 8 2 5
			// ex: 3 1 m4 6 7 .8 2 5
			// ex: 3 1 m4 6 7 8 .2 5
			// ex: 3 1 4 m6 7 8 .2 5
			// ex: 3 1 4 m2 7 8 .6 5

		}
		temp = arr[i + 1];
		arr[i + 1] = arr[high]; 
		arr[high] = temp;
		return (i + 1);
	}
}
