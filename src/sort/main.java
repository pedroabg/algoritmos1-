package sort;

public class main {

    public static void main(String[] args) {
        int[] arr = {355,728,498,48,967,350,299,851,175,179,415,826,251,630};
        Bucketsort.sort(arr,967);
        printA(arr);

        int[] arr2 = {87,98,87,55,80,37,71,8,2,20,92,56,31,65,89,22,38,40,83,84};
        Quick.quickSort(arr2, 0, arr2.length-1);
        printA(arr2);
        int[] arr3 = {87,98,87,71,8,2,1,55,80,37,92,56,31,65,3,22,38,40,83,84};
        Merge.mergeSort(arr3,arr3.length);
        printA(arr3);

    }

    private static void printA(int[] arr) {
        System.out.println("");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
    }


}
